import pickle, json, csv, sys, os



class Inputs:

    def __init__(self):
        args = sys.argv[1:]
        try:
            self.src, self.dst, self.values = args[0], args[1], args[2:]
            print(self.values)
        except:
            print("Blad!")
            print(f'Liczba podanych argumewntow: {len(args)} Minimalna liczba: 2')
        




class FileRecognize(Inputs):

    def __init__(self):
        super().__init__()


        if self.is_pickle():
            print("PICKLE!")
            PickleFile(self.src, self.dst, self.values)
        
        if self.is_json():
            print("JSON!")
            JsonFile(self.src, self.dst, self.values)

        if self.is_csv() and not (self.is_json() or self.is_pickle()):
            print("CSV!")
            CsvFile(self.src, self.dst, self.values)
        
        else:
            print("No csv file in this folder")
            self.list_files()
            


    def is_pickle(self):
        try:
            with open(self.src, "rb") as file:
                pickle.load(file)
            return True
        except:
            return False


    def is_json(self):
        try:
            with open(self.src, "rb") as file:
                json.load(file)
            return True
        except:
            return False
    

    def is_csv(self):
        try:
            with open(self.src, "rb") as file:
                csv.reader(file)
            return True
        except:
            return False
    
    def list_files(self):
        path = "/".join(self.src.split('/')[0:-1])
        files = os.listdir(path)
        print("Pliki we wskazanym folderze: \n")
        for file in files:
            print(file)

        
        

class CsvFile:

    def __init__(self, src, dst, values):
        self.src = src
        self.dst = dst
        self.values = values

        
        self.read()

    def read(self):
        with open(self.src) as file:
            for line in file:
                print(line)






class JsonFile:
    def __init__(self, src, dst, values):
        self.src = src
        self.dst = dst
        self.values = values
        self.read()

    def read(self):
        with open(self.src, "rb") as file:
            print(json.load(file))




class PickleFile:
    def __init__(self, src, dst, values):
        self.src = src
        self.dst = dst
        self.values = values

        self.edit()
        self.open_new()


    def edit(self):
        with open(self.src, "rb") as file:
            lines = pickle.load(file)

        for value in self.values:
            column, row, comment = value.split(",")
            lines[int(column)][int(row)] = comment 

        with open(self.src, "wb") as file:
            pickle.dump(lines, file)

    
    def open_new(self):
        with open(self.src, "rb") as file:
            print(pickle.load(file))
        


        







if __name__ == "__main__":
    FileRecognize()

