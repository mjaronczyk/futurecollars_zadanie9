import pickle, json, csv, sys, os, re

class Inputs:

    def __init__(self):
        args = sys.argv[1:]
        try:
            path_src, path_dst, self.values = args[0], args[1], args[2:]
        except:
            print("Blad!")
            print(f'Liczba podanych argumewntow: {len(args)} Minimalna liczba: 2')
        self.src = re.sub(r"\\", "/", path_src)
        self.dst = re.sub(r"\\", "/", path_dst)
        print(self.src)



class FileRecognize(Inputs):

    def __init__(self):
        super().__init__()


        if self.is_pickle():
            print("PICKLE!")
            PickleFile(self.src, self.dst, self.values)
            

        
        elif self.is_json():
            print("JSON!")


        elif self.is_csv() and not (self.is_json() or self.is_pickle()):
            print("CSV!")
            CsvFile(self.src, self.dst, self.values)

        else:
            print("\nNo csv file in this folder")
            self.list_files()
            


    def is_pickle(self):
        try:
            with open(self.src, "rb") as file:
                pickle.load(file)
            return True
        except:
            return False


    def is_json(self):
        try:
            with open(self.src, "rb") as file:
                json.load(file)
            return True
        except:
            return False
    

    def is_csv(self):
        try:
            with open(self.src, "rb") as file:
                csv.reader(file)
            return True
        except:
            return False
    

    def list_files(self):
        path = "/".join(self.src.split('/')[0:-1])
        files = os.listdir(path)
        print("Existing files in folder: \n")
        for file in files:
            print(file)



class PickleFile:
    def __init__(self, src, dst, values):
        self.src = src
        self.dst = dst
        self.values = values
        self.edit()
        self.open_new()

    def edit(self):
        with open(self.src, "rb") as file:
            lines = pickle.load(file)

        for value in self.values:
            column, row, comment = value.split(",")
            lines[int(column)][int(row)] = comment 

        with open(self.dst, "wb") as file:
            pickle.dump(lines, file)

    def open_new(self):
        with open(self.dst, "rb") as file:
            print(pickle.load(file))



class CsvFile:

    def __init__(self, src, dst, values):
        self.src = src
        self.dst = dst
        self.values = values
        self.read()

    def read(self):
        with open(self.src) as file:
            lines = file.readlines()

        for value in self.values:
            column, row, comment = value.split(",")  
            line = lines[int(column)].split(' ')
            line[int(row)] = comment
            str =" ".join(line)
            lines[int(column)] = str
            print(lines)
        
        with open(self.dst, "w") as file:
            file.writelines(lines)

        
            
            








if __name__ == "__main__":
    FileRecognize()




